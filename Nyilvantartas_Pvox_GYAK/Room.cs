﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nyilvantartas_Pvox_GYAK
{
    public class Room
    {
        public static List<Room> Szobalista = new List<Room>();
        public Room(int number)
        {
            Number = number;
        }
        private int _number;
        public int Number
        {
            get
            {
                return _number;
            }
            set
            {
                if (value >= 1 && value <= 50) _number = value;
                else _number = 1;
            }
        }
    }
}
