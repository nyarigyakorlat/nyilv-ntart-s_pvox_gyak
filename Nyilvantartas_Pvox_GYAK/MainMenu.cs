﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nyilvantartas_Pvox_GYAK
{
    class MainMenu
    {
        public int Menu()
        {
            int szam = 0;
            do
            {
                Console.SetCursorPosition(50, 1);
                Console.WriteLine("Pentavox Nyilvántartás");
                Console.SetCursorPosition(30, 3);
                Console.WriteLine("1. Alkalmazottak");
                Console.SetCursorPosition(30, 4);
                Console.WriteLine("2. Szobák szerint alkalmazottak");
                Console.SetCursorPosition(30, 5);
                Console.WriteLine("0. Kilépés");
                Console.SetCursorPosition(20, 8);
                Console.Write("A választott menü száma: ");
                int.TryParse(Console.ReadLine(), out szam);
                Console.Clear();
                if (szam >= 0 && szam <= 2) break;
            } while (true);
            return szam;
        }
    }
}
