﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nyilvantartas_Pvox_GYAK
{
    class Program
    {
        static void Alkalmazottak()
        {
            //Szobák emberek létrehozása
            
            Room room25 = new Room(25);
            Room room28 = new Room(28);
            Room room27 = new Room(27);
            Room room29 = new Room(29);
            Room.Szobalista.Add(room25);
            Room.Szobalista.Add(room27);
            Room.Szobalista.Add(room28);
            Room.Szobalista.Add(room29);            
            Personal Szabolcs = new Personal("Nagy Szabolcs", Nem.Male, room28);
            Personal.Alkalmazottak.Add(Szabolcs);
            Personal Dávid = new Personal("Molnár Dávid", Nem.Male, room28);
            Personal.Alkalmazottak.Add(Dávid);
            Personal Tomi = new Personal("Ragyelik Tamás", Nem.Male, room28);
            Personal.Alkalmazottak.Add(Tomi);
            Personal Gyula = new Personal("Gyula", Nem.Male, room27);
            Personal.Alkalmazottak.Add(Gyula);
            Personal Milka = new Personal("Milka", Nem.Female, room29);
            Personal.Alkalmazottak.Add(Milka);
            Personal Szöcske = new Personal("Szöcske", Nem.Male, room29);
            Personal.Alkalmazottak.Add(Szöcske);
            Personal Tibor = new Personal("Tibor", Nem.Male, room28);
            Personal.Alkalmazottak.Add(Tibor);
            Personal János = new Personal("János", Nem.Male, room25);
            Personal.Alkalmazottak.Add(János);
        }
        static void Menu1(string szoveg)
        {
            Console.WriteLine(szoveg);
            //Kiírás
            for (int i = 0; i < Personal.Alkalmazottak.Count; i++)
            {
                Personal.Alkalmazottak[i].Print(i + 1, 20, 3 + i);
            }
        }
        static void Menu5(string szoveg)
        {
            //Szobák szerint kiírás 5. MENÜ
            Console.WriteLine(szoveg);
                int i = 2;
                foreach (var room in Room.Szobalista)
                {
                    i++;
                    Console.SetCursorPosition(20, i);
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine(room.Number + " Szoba");
                    i++;
                    foreach (var ember in Personal.Alkalmazottak)
                    {
                        if (ember.Room != room) continue;
                        else
                        {
                            i++;
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.SetCursorPosition(20, i);
                            Console.Write(ember.Name + " ");
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write(ember.Sex);

                        }
                    }
                    i++;
                }
                Console.ForegroundColor = ConsoleColor.Gray;
        }

        static void Main(string[] args)
        {
            MainMenu Menu = new MainMenu();
            Alkalmazottak();
            do
            {
                int selectedMenu = Menu.Menu();
                Console.SetCursorPosition(50, 1);
                switch (selectedMenu)
                {
                    case 1:
                        Menu1("Minden adat az alkalmazottakról, s szobáikról");
                        break;
                    case 2:
                        Menu5("Szobák szerint, alkalmazottak");
                        break;
                    case 0:
                        return;
                }
                
                Console.WriteLine("\n\n\nVisszalépéshez nyomj meg egy gombot...");
                Console.ReadKey();
                Console.Clear();
            } while (true);
        }
    }
}
