﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nyilvantartas_Pvox_GYAK
{
    enum Nem
    {
        Male, Female
    }
    class Personal
    {
        public static List<Personal> Alkalmazottak = new List<Personal>();
        public override string ToString()
        {
            return string.Format($"{Name}, {Sex}");
        }
        public Room Room
        {
            get; private set;
        }
        public string Name;
        public string Sex;
        public Personal(string name, Nem nvf, Room room)
        {
            Room = room;
            Name = name;

            switch (nvf)
            {
                case Nem.Male:
                    Sex = "Férfi";
                    break;
                case Nem.Female:
                    Sex = "Nő";
                    break;
                default:
                    break;
            }

        }
        public void Print(int sorszam, int x, int y)
        {
            Console.SetCursorPosition(x, y);
            Console.Write(sorszam + ".");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write(Name + " ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(Sex + " ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(Room.Number);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
